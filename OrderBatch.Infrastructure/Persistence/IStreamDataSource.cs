﻿using System.IO;

namespace OrderBatch.Persistence
{
    public interface IStreamDataSource
    {
        StreamReader Open();
    }
}