﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace OrderBatch.Persistence
{
    public class OrderBatchContext : IOrderBatchContext
    {
        private readonly IStreamDataSource _streamDataSource;

        public OrderBatchContext(IStreamDataSource streamDataSource)
        {
            _streamDataSource = streamDataSource;
        }

        public IEnumerable<(string BoutiqueId, string OrderId, decimal TotalOrderPrice)> Set()
        {
            using (var streamReader = _streamDataSource.Open())
                while (!streamReader.EndOfStream)
                {
                    var line = streamReader.ReadLine();
                    if (!String.IsNullOrWhiteSpace(line))
                    {
                        var values = line.Split(',');
                        if (values.Length > 2)
                        {
                            (string BoutiqueId, string OrderId, decimal TotalOrderPrice) item;

                            item.BoutiqueId = values[0];
                            item.OrderId = values[1];
                            Decimal.TryParse(values[2], NumberStyles.Any, CultureInfo.InvariantCulture, out item.TotalOrderPrice);

                            yield return item;
                        }
                    }
                }
        }
    }
}