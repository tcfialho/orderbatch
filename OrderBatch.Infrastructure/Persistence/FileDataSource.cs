﻿using System.IO;

namespace OrderBatch.Persistence
{
    public class FileDataSource : IStreamDataSource
    {
        private readonly string _pathToFile;

        public FileDataSource(string pathToFile)
        {
            _pathToFile = pathToFile;
        }

        public StreamReader Open()
        {
            return new StreamReader(_pathToFile);
        }
    }
}