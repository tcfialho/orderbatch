﻿using System.Collections.Generic;

namespace OrderBatch.Persistence
{
    public interface IOrderBatchContext
    {
        IEnumerable<(string BoutiqueId, string OrderId, decimal TotalOrderPrice)> Set();
    }
}