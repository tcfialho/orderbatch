﻿using System.Collections.Generic;
using System.Linq;
using OrderBatch.Domain;
using OrderBatch.Domain.Repositories;
using OrderBatch.Persistence;

namespace OrderBatch.Infrastructure.Repositories
{
    public class BoutiqueRepository : IBoutiqueRepository
    {
        private readonly IOrderBatchContext _context;

        public BoutiqueRepository(IOrderBatchContext context)
        {
            _context = context;
        }

        public IEnumerable<Boutique> GetAll()
        {
            return _context.Set().GroupBy(x => x.BoutiqueId,
                                          y => new Order(y.OrderId, y.TotalOrderPrice))
                                 .Select(g => new Boutique(g.Key, g.ToList()));
        }
    }
}