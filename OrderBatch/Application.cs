﻿using OrderBatch.Domain.Repositories;
using System;

/// <summary>
/// Autor:
///  Thiago de Castro Fialho - tcfialho@gmail.com
///  
/// Subject:
///  Order batch processing
///
///  Implement a console application that will calculate the total commissions that Farfetch should charge for each boutique on a given day, with the following rules:
///   a.) Boutiques should be charged by 10% of the total value every order
///   b.) The order with the highest value of the day will not be subject to commission
///  
///  Given the input `orders.csv` file:
///   B10,O1000,100.00
///   B11,O1001,100.00
///   B10,O1002,200.00
///   B10,O1003,300.00
///
///  The result should be:
///   B10,30
///   B11,10   <- When the boutique has a single order the rule b is ignored.
/// </summary>
namespace OrderBatch
{
    public class Application
    {
        private readonly IBoutiqueRepository _boutiqueRepository;

        public Application(IBoutiqueRepository boutiqueRepository)
        {
            _boutiqueRepository = boutiqueRepository;
        }

        public void Run()
        {
            foreach (var item in _boutiqueRepository.GetAll())
            {
                Console.WriteLine($"{item.Id}, {item.TotalComission():0.##}");
            }
        }
    }
}