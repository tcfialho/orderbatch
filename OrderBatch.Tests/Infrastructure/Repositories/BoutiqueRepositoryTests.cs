﻿using Moq;
using OrderBatch.Infrastructure.Repositories;
using OrderBatch.Persistence;
using System.Linq;
using Xunit;

namespace OrderBatch.Tests.Infrastructure.Repositories
{
    public class BoutiqueRepositoryTests
    {
        [Fact]
        public void TheGetAllShouldReturnUniqueBoutiques()
        {
            //Arrange
            var orderBatchContext = new Mock<IOrderBatchContext>();

            orderBatchContext.Setup(x => x.Set()).Returns(new (string BoutiqueId, string OrderId, decimal TotalOrderPrice)[]
            {
                (BoutiqueId: "B10", OrderId: "O1000", TotalOrderPrice: 100.00M),
                (BoutiqueId: "B11", OrderId: "O1001", TotalOrderPrice: 100.00M),
                (BoutiqueId: "B11", OrderId: "O1002", TotalOrderPrice: 100.00M),
                (BoutiqueId: "B10", OrderId: "O1003", TotalOrderPrice: 200.00M),
            });

            var boutiqueRepository = new BoutiqueRepository(orderBatchContext.Object);

            //Act
            var boutiques = boutiqueRepository.GetAll();

            //Assert
            Assert.DoesNotContain(boutiques.GroupBy(x => x.Id), group => group.Count() > 1);
        }

        [Fact]
        public void TheGetAllShouldReturnUniqueBoutiquesWithOrders()
        {
            //Arrange
            var orderBatchContext = new Mock<IOrderBatchContext>();

            orderBatchContext.Setup(x => x.Set()).Returns(new (string BoutiqueId, string OrderId, decimal TotalOrderPrice)[]
            {
                (BoutiqueId: "B10", OrderId: "O1000", TotalOrderPrice: 100.00M),
                (BoutiqueId: "B10", OrderId: "O1001", TotalOrderPrice: 200.00M),
                (BoutiqueId: "B10", OrderId: "O1002", TotalOrderPrice: 300.00M),
            });

            var boutiqueRepository = new BoutiqueRepository(orderBatchContext.Object);

            //Act
            var boutiques = boutiqueRepository.GetAll();

            //Assert
            Assert.NotEmpty(boutiques.First().Orders);
        }
    }
}