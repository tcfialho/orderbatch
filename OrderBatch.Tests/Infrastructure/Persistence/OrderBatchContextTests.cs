﻿using Moq;
using OrderBatch.Persistence;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;

namespace OrderBatch.Tests.Infrastructure.Persistence
{
    public class OrderBatchContextTests
    {
        [Fact]
        public void TheOrderBatchContextShouldReturnValues()
        {
            //Arrange
            var content = "B10,O1000,100.00\r\nB11, O1001, 100.00\r\nB10, O1002, 200.00\r\nB10, O1003, 300.00";

            var fileDataSource = new Mock<IStreamDataSource>();

            fileDataSource.Setup(x => x.Open()).Returns(() => {
                var stream = new MemoryStream(Encoding.UTF8.GetBytes(content));
                var streamReader = new StreamReader(stream);
                return streamReader;
            });

            var orderBatchContext = new OrderBatchContext(fileDataSource.Object);

            //Act
            var boutiques = orderBatchContext.Set().ToList();

            //Assert
            Assert.NotEmpty(boutiques);
        }
    }
}
