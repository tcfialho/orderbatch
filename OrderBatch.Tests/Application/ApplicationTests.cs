﻿using Moq;
using OrderBatch.Domain;
using OrderBatch.Domain.Repositories;
using System;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace OrderBatch.Tests.Application
{
    public class ApplicationTests
    {
        private readonly TestOutputHelper output;

        public ApplicationTests(ITestOutputHelper output)
        {
            this.output = (TestOutputHelper)output;
        }

        [Fact]
        public void TheGetAllShouldReturnUniqueBoutiques()
        {
            //Arrange
            var boutiqueRepository = new Mock<IBoutiqueRepository>();

            boutiqueRepository.Setup(x => x.GetAll()).Returns(new Boutique[]
            {
                new Boutique("B10", new Order[] { new Order("O1000", 100.00M), new Order("O1002", 200.00M), new Order("O1003", 300.00M) }),
                new Boutique("B11", new Order[] { new Order("O1001", 100.00M) })
            });

            Console.SetOut(new XUnitTestConsoleConverter(output));

            var app = new OrderBatch.Application(boutiqueRepository.Object);

            //Act
            app.Run();

            //Assert
            Assert.Equal("B10, 30\r\nB11, 10\r\n", output.Output);
        }
    }
}