﻿using OrderBatch.Model;
using System;
using Xunit;

namespace OrderBatch.Tests.Application
{
    public class OptionsTests
    {
        [Fact]
        public void TheOptionsShouldThrowsArgumentException()
        {
            //Arrange
            var args = new string[] { "OrderBatch.dll" };

            //Act
            //Assert
            Assert.Throws<ArgumentException>(() => new Options(args));
        }

        [Fact]
        public void TheOptionsShouldReturnPathToFile()
        {
            //Arrange
            var options = new Options(new string[] { "OrderBatch.dll", "orders.csv" });

            //Act
            var pathToFile = options.PathToFile;

            //Assert
            Assert.Equal("orders.csv", pathToFile);
        }
    }
}
