﻿using OrderBatch.Domain;
using Xunit;

namespace OrderBatch.Tests
{
    public class BoutiqueTests
    {
        [Fact]
        public void TheSingleOrderAwaysWillBeSubjectToCommission()
        {
            // Arrange
            var boutique = new Boutique("1", new Order[] { new Order("2", 100M) });

            // Act
            var totalComission = boutique.TotalComission();

            // Assert
            Assert.Equal(10, totalComission);
        }

        [Fact]
        public void TheOrderWithTheHighestValueWillNotBeSubjectToCommission()
        {
            // Arrange
            var boutique = new Boutique("1", new Order[] { new Order("2", 100M), new Order("2", 200M), new Order("2", 300M) });

            // Act
            var totalComission = boutique.TotalComission();

            // Assert
            Assert.Equal(30, totalComission);
        }
    }
}
