﻿using System.Collections.Generic;

namespace OrderBatch.Domain.Repositories
{
    public interface IBoutiqueRepository
    {
        IEnumerable<Boutique> GetAll();
    }
}