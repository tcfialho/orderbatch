﻿using System.Collections.Generic;
using System.Linq;

namespace OrderBatch.Domain
{
    public class Boutique
    {
        public string Id { get; }
        public IReadOnlyCollection<Order> Orders { get; }

        public Boutique(string id, IReadOnlyCollection<Order> orders)
        {
            Id = id;
            Orders = orders;
        }

        public decimal TotalComission()
        {
            var commissionRate = 0.1m;

            var totalComisionable = (Orders.Count == 1) ?
                                        Orders.Sum(x => x.TotalOrderPrice) :
                                        Orders.OrderByDescending(x => x.TotalOrderPrice).Skip(1).Sum(x => x.TotalOrderPrice);

            return totalComisionable * commissionRate;
        }
    }
}