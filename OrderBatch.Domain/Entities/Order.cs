﻿namespace OrderBatch.Domain
{
    public class Order
    {
        public string Id { get; set; }
        public decimal TotalOrderPrice { get; set; }
        public Order(string id, decimal totalOrderPrice)
        {
            Id = id;
            TotalOrderPrice = totalOrderPrice;
        }
    }
}